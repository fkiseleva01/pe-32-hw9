
const tab = function() {
   const tabNav = document.querySelectorAll(".tabs-title")
    const tabContent = document.querySelectorAll(".tabs-content li")
    let tabTarget = ""
   

    tabNav.forEach(item => {
        item.addEventListener("click", selectTabNav)
    })

    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove("active")
        })
        this.classList.add("active")
        tabTarget = this.getAttribute("data-tab-name")
        selectTabContent(tabTarget)
      
    }
    
    function selectTabContent(tabTarget) {
        tabContent.forEach(el => {
            if(el.getAttribute("data-tab-name") === tabTarget){
                el.classList.add("active")
            } else {
                el.classList.remove("active")
            }
        })

    }
  
}
tab()
